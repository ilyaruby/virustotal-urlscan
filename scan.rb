require 'uirusu'
require 'yaml'

class UrlScanner
  CONFIG_FILE = 'config/virustotal.yml'
  MAX_ATTEMPTS = 10
  def initialize
    @config = YAML.load_file(CONFIG_FILE)
    @api_key = @config["apikey"]
  end
  def load(urls)
    @urls = urls
  end
  def load_dataset
    read(@config["dataset"])
  end
  def read(f)
    @urls = File.read(f).split('\n').map(&:strip)
  end
  def is_phishing?(scans)
    scans.select {|src, res| res["detected"]}.select {|src, res| res["result"].include?('phishing')}.size > 0
  end
  def verdict(result)
    return "Scan error" unless result["response_code"] == 1
    return "Phishing" if is_phishing?(result["scans"])  
    return "Not phishing"
  end
  def scan
    attempts = 0
    begin
      @results = Uirusu::VTUrl.query_report(@api_key, @urls.join("\n"))
    rescue RuntimeError => e
      print "Cannot scan. Got error: #{e}"
      attempts += 1
      return false unless attempts < MAX_ATTEMPTS
      print "Will wait for 10 seconds and try again".
      sleep 10
      retry
    end
    true
  end
  def print_result
    unless @results
      print "No results to print."
      return false
    end
    @results.each do |result|
      url = result["url"]
      verdict = verdict(result)
      puts "#{verdict}: #{url}"
    end
  end
end

#urls = [ "http://girdharmahal.com/Newdropbox/",
#"http://sicemx.com/wp-includes/fonts/web.php",
#"http://ya.ru/",
#"http://google.com/",
#"http://citibank.ru/" ]
#scanner.load urls

scanner = UrlScanner.new
scanner.load_dataset
scanner.scan
scanner.print_result


